# MAPA CONCEPTUAL NO.1: LA CONSULTORÍA
```plantuml
@startmindmap
*[#LightCoral]  **La consultoria**
	**[#LightBlue] ¿Qué es?
		*[#LightYellow] Empresa de servicios profesionales \n de alto valor añadido
			*_ tiene
				*[#LightGreen] 3 características esenciales
					*[#LightYellow] Conocimiento sectorial
					*[#LightYellow] Conocimiento técnico
					*[#LightYellow] Capacidad de plantear soluciones
	*_ nace de
		*[#LightBlue] Las grandes organizaciones
			*_ que tienen
				*[#LightGreen] Gran complejidad interna
					*_ y necesitan
						*[#LightYellow] Procesos y sistemas \n de información complejos
			*_ en general no pueden
				*[#LightGreen] Operar y transformarse
					*_ debido a
						*[#LightYellow] Falta de tiempo
						*[#LightYellow] Falta de personal cualificado
						*[#LightYellow] Falta de conocimiento específico
	*[#LightBlue] ¿Qué tipo de servicios presta?
		*[#LightGreen] consultoria
			*_ abarca
				*[#GreenYellow] Reigeniería de Procesos
				*[#GreenYellow] Gobierno IT
					*_ tiene que ver con
						*[#LightYellow] Calidad
						*[#LightYellow] Métodos de desarrollo
						*[#LightYellow] Atender cambios
				*[#GreenYellow] Oficina de proyectos
					*_ necesitan
						*[#LightYellow] Buena gestión de proyectos 
				*[#GreenYellow] Analítica avanzada de datos
					*_ es importante
						*[#LightYellow] Información en tiempo real
						*[#LightYellow] Saber qué hacer con la información
						*[#LightYellow] Big Data
		*[#LightGreen] Integración
			*[#LightYellow] Meter piezas de \n software o hardware en el mapa \n de sistemas de la compañía
				*_ utilizando
					*[#GreenYellow] Desarrollos a medida
						*[#LightYellow] Aplicar tecnologías adecuadas
					*[#GreenYellow] Aseguramiento de \n la calidad de software
						*[#LightYellow] Intervenir en su solución diferentes sistemas
					*[#GreenYellow] Infraestructura
						*[#LightYellow] Uso de la plataforma
					*[#GreenYellow] Soluciones de mercado
						*[#LightYellow] Entender lo que demanda \n la problemática del cliente
						*[#LightYellow] Ofrecer solución viable
		*[#LightGreen] Externalización
			*[#LightYellow] Proceso por el cual una empresa encomienda \n la realización de una parte de sus tareas o servicios a otra empresa
				*_ por ejemplo
					*[#GreenYellow] Gestión de aplicaciones
						*[#LightYellow] Las empresas deciden delegar \n el mantenimiento o evolución de sus sistemas
					*[#GreenYellow] Servicios SQA 
						*[#LightYellow] Conjunto de actividades planificadas \n y sistemáticas cuyo objetivo es evaluar la calidad
	*[#LightBlue] ¿Por qué es la profesión del futuro?
		*_ la consultoría tiene \n un papel clave como
			*[#LightGreen] Proveedor de servicios de habilitación
				*_ estos tienen
					*[#GreenYellow] Un papel clave
						*_ en
							*[#LightYellow] Transformación digital
							*[#LightYellow] Industria 4.0
							*[#LightYellow] Smartcities
	*[#LightBlue] ¿Qué exige la consultoría?
		*[#LightGreen] Disponibilidad total
			*[#LightYellow] Nueva forma de ver la vida
			*[#LightYellow] Trabajar horas de más
		*[#LightGreen] Nuevos retos
			*[#LightYellow] Actuar más allá
			*[#LightYellow] Algo de estrés
		*[#LightGreen] Viajes
			*[#LightYellow] La mobilidad geográfica es demandante
		*[#LightGreen] Buenos profesionales 
			*_ por su 
				*[#LightYellow] Formación y experiencia
				*[#LightYellow] Capacidad de trabajo y evolución
			*_ capaces de
				*[#LightYellow] Trabajar en equipo
				*[#LightYellow] Ser honestos
				*[#LightYellow] Colaborar en proyectos
	*[#LightBlue] ¿Cómo es la carrera profesional en consultoría?
		*[#LightGreen] Junior
			*[#LightYellow] Asistente
			*[#LightYellow] Consultor junior
			*[#LightYellow] Consultoría
		*[#LightGreen] Senior 
			*[#LightYellow] Consultor Senior
			*[#LightYellow] Jefe de equipo
			*[#LightYellow] Jefe de proyecto
		*[#LightGreen] Gerente
		*[#LightGreen] Director
@endmindmap
```mindmap
```
# MAPA CONCEPTUAL NO.2: CONSULTORÍA DE SOFTWARE
```plantuml
@startmindmap
*[#LightCoral]  **Consultoría de software**
	*_ principal actividad que lleva a cabo 
		*[#LightBlue] Desarrollo de software
			*_ se hace en
				*[#LightGreen] Factorias de software
					*[#LightYellow] Centros especializados de construcción de software
			*_ donde va al frente un 
				*[#LightGreen] Jefe de proyecto 
					*[#LightYellow] Persona encargada de planificar las fases del proyecto
					*[#LightYellow] Hablar con el cliente
					*[#LightYellow] Coordinar las tareas
					*[#LightYellow] Debe tener una metodología
					
			*_ se hace a través de 
				*[#LightGreen] Proceso
					*[#LightYellow] Comienza cuando un cliente \n tiene una necesidad
						*_ por ejemplo
							*[#LightYellow] Tener que vender un producto en el mercado
							*[#LightYellow] Quiere una aplicación para almacenar clientes
							*[#LightYellow] Almacenar información para ser ágil y productivo
				*[#LightGreen] 1. La consultoría 
					*[#LightYellow] Hablar de ventajas
					*[#LightYellow] ¿Tiene sentido el software?
					*[#LightYellow] ¿Es viable?                   
				*[#LightGreen] 2. Diseño funcional
					*[#LightYellow] Información de ENTRADA/SALIDA
					*[#LightYellow] Saber el modelo de datos
					*[#LightYellow] Prototipo
					*[#LightYellow] Tener personas que hablen el lenguaje del cliente            
				*[#LightGreen] 3. Diseño técnico
					*[#LightYellow] Se traslada todo a un lenguaje de programación y una base de datos
				*[#LightGreen] 4. Pruebas
					*[#LightYellow] Se hacen pruebas intregradas y funcionales
						*_ una vez terminadas 
							*[#LightYellow] Se instala el software en \n el área que el cliente indique
		*[#LightBlue] Actividades Desarrolladas
			*[#LightGreen] ¿Qué necesita una empresa para funcionar y mantenerse?
				*_ debemos asegurarnos que
					*[#GreenYellow] La infraestructura sea funcional
						*[#LightYellow] Ordenadores
						*[#LightYellow] Servidores
						*[#LightYellow] Habilitar infraestructa de comunicación
						*[#LightYellow] Seguridad
			*[#LightGreen] ¿Cómo hacer que una empresa vuelva a funcionar?
				*_ la consultoría debe estar detrás para
					*[#LightYellow] Conseguir que la infraestructura de comunicación funcione
					*[#LightYellow] Asegurarse que el software no tenga problemas o fallos
			*[#LightGreen] Comprender el factor de escala
				*[#LightYellow] Es necesario tener una comprensión y una visión del entorno
				*[#LightYellow] No conocer solamente informática
				*[#LightYellow] Tener bien en claro las necesidades del cliente
				*[#LightYellow] Saber que todos los sectores tienen procesos críticos
		*[#LightBlue] Vista Técnica de AXPE
			*_ esta empresa
				*[#LightYellow] Es una organización nacida en Madrid
				*[#LightYellow] Presta servicios directamente en 4 países
				*[#LightYellow] Presta servicios indirectamente en 20 países
			*_ desarrolla
				*[#LightGreen] Software avanzado
				*[#LightGreen] Servicios de consultoria
		 	*_ utilizan
			 	*[#LightGreen] Mecánicas y métodos avanzados
				 	*_ para
					 	*[#LightYellow] Prestar servicios de alto valor
@endmindmap
```mindmap
```
# MAPA CONCEPTUAL NO.3: APLICACIÓN DE LA INGENIERÍA DE SOFTWARE
```plantuml
@startmindmap
*[#LightCoral]  **Aplicación de la** \n** ingeniería de software**
	*_ existen empresas como 
		*[#LightBlue] Stratesys
			*_ cuyas señas de
				*[#LightGreen] Identidad
					*_ son
						*[#GreenYellow] Conocimiento
						*[#GreenYellow] Innovación
						*[#GreenYellow] Especialización
						*[#GreenYellow] Calidad
	*_ debe tener un
		*[#LightBlue] Enfoque metodológico
			*_ que algunas veces es
				*[#LightGreen] Standard
					*[#LightYellow] Va desde recoger los requistos \n hasta generar un documento
						*_ para que se convierta en un
							*[#LightYellow] Plan de proyecto
					*[#LightYellow] Reunirse con todas las áreas de sistemas
					*[#LightYellow] Visión Global
					*[#LightYellow] Entender los procesos
			*_ algunos pasos son
				*[#LightGreen] Recaudar las necesidades del cliente
				*[#LightGreen] Diesño Funcional
				*[#LightGreen] Entender los pasos a realizar
				*[#LightGreen] Análisis Técnico
					*[#LightYellow] ¿Cómo se harán las cosas?
						*[#LightYellow] Programación
						*[#LightYellow] Parametrización
	*_ podemos dividirlo sencillamente en 
		*[#LightBlue] Tres fases
			*[#LightGreen] Fase de análisis
				*_ corresponde
					*[#LightYellow] Saber y entender los requerimientos
			*[#LightGreen] Fase de construcción
				*_ se trata de
					*[#LightYellow] La etapa de desarrollo con los requeimientos solicitados
			*[#LightGreen] Fase de pruebas
				*_ consiste en
					*[#LightYellow]  Pruebas para la detección y corrección de errores
@endmindmap
```mindmap

```
## Material utilizado (referencias)

1. La consultoría
	- https://canal.uned.es/video/5a6f887cb1111f81638b4570

2. Consultoría de Software
	- https://canal.uned.es/video/5a6f1733b1111f35718b4580
	- https://canal.uned.es/video/5a6f1734b1111f35718b4586
	- https://canal.uned.es/video/5a6f1730b1111f35718b456a
	- https://canal.uned.es/video/5a6f1734b1111f35718b458b

3. Aplicación de la ingeniería de software
	- https://canal.uned.es/video/5a6f4c4eb1111f082a8b4978
